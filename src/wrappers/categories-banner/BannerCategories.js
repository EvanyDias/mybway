import PropTypes from "prop-types";
import React from "react";
import { Link } from "react-router-dom";

const BannerCategories = ({ spaceTopClass, spaceBottomClass }) => {
  return (
    <div
      className={`banner-area ${spaceTopClass ? spaceTopClass : ""}  ${
        spaceBottomClass ? spaceBottomClass : ""
      }`}
    >
      <div className="container-wrap banner-categories padding-20-row-col">
        <div className="row">
          <div className="col-lg-6 col-md-6">
            <div className="single-banner mb-20">
              <Link to={process.env.PUBLIC_URL + "/shop-grid-standard"}>
                <img className="box1"
                  src={
                    process.env.PUBLIC_URL + "/assets/img/banner/box1.png"
                  }
                  alt=""
                />
              </Link>
              <div className="banner-content-4 banner-position-hm15-2 pink-banner">
               {/*Texto Banner*/}
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 col-box-2">
            <div className="single-banner mb-20">
              <Link to={process.env.PUBLIC_URL + "/shop-grid-standard"}>
                <img
                  src={
                    process.env.PUBLIC_URL + "/assets/img/img/box2.png"
                  }
                  alt=""
                />
              </Link>
              <div className="banner-content-3 banner-position-hm15-2 pink-banner">
                {/*Texto Banner*/}
              </div>
            </div>
            <div className="single-banner mb-20">
              <Link to={process.env.PUBLIC_URL + "/shop-grid-standard"}>
                <img
                  src={
                    process.env.PUBLIC_URL + "/assets/img/banner/box3.png"
                  }
                  alt=""
                />
              </Link>
              <div className="banner-content-3 banner-position-hm17-1 pink-banner">
               
              
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 col-box-2">
            <div className="single-banner mb-20">
              <Link to={process.env.PUBLIC_URL + "/shop-grid-standard"}>
                <img
                  src={
                    process.env.PUBLIC_URL + "/assets/img/img/box4.png"
                  }
                  alt=""
                />
              </Link>
              <div className="banner-content-3 banner-position-hm15-2 pink-banner">
              
               
              </div>
            </div>
            <div className="single-banner mb-20">
              <Link to={process.env.PUBLIC_URL + "/shop-grid-standard"}>
                <img
                  src={
                    process.env.PUBLIC_URL + "/assets/img/banner/box5.png"
                  }
                  alt=""
                />
              </Link>
              <div className="banner-content-3 banner-position-hm17-1 pink-banner">
               
              
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

BannerCategories.propTypes = {
  spaceBottomClass: PropTypes.string,
  spaceTopClass: PropTypes.string
};

export default BannerCategories;
