import React, { Fragment } from "react";
import MetaTags from "react-meta-tags";
import LayoutOne from "../../layouts/LayoutOne";
import HeroSliderOne from "../../wrappers/hero-slider/HeroSliderOne";
import MenuCategories from "../../wrappers/menu-categories/MenuCategories";
import FeatureIcon from "../../wrappers/feature-icon/FeatureIcon";
import TabProduct from "../../wrappers/product/TabProduct";
import BannerCategories from "../../wrappers/categories-banner/BannerCategories";
import TabProductEleven from "../../wrappers/product/TabProductEleven";
import TabProductsOur from "../../wrappers/product/TabProductsOur";
import Newsletter from "../../wrappers/newsletter/Newsletter";
import HeroSliderAdverts from "../../wrappers/hero-slider/HeroSliderAdverts";
const Home = () => {
  return (
    <Fragment>
      <MetaTags>
        <title>MybWay</title>
        <meta
          name="description"
          content="MybWay"
        />
      </MetaTags>
      <LayoutOne
        headerContainerClass="container-fluid"
        headerPaddingClass="header-padding-1"
      >
        {/* Menu de Categorias */}
        <MenuCategories />

        <HeroSliderOne />

        {/* Banners Categories */}
        <BannerCategories />

        <TabProductEleven
          category="kids"
          spaceTopClass="pt-100"
          spaceBottomClass="pb-100"
          sectionTitle="Featured Products"
          bgColorClass="bg-gray-3"
        />
           <TabProductsOur
          category="kids"
          spaceTopClass="pt-100"
          spaceBottomClass="pb-100"
          sectionTitle="Featured Products"
          bgColorClass="bg-gray-3"
        />
        {/* tab product */}
        <TabProduct spaceBottomClass="pb-60" category="fashion" />
   <HeroSliderAdverts />
        {/* Newsletter */}
        <Newsletter />

        {/* featured icon */}
        <FeatureIcon spaceTopClass="pt-100" spaceBottomClass="pb-60" />
           </LayoutOne>
    </Fragment>
  );
};

export default Home;
