import PropTypes from "prop-types";
import React from "react";

const FeatureIconSingle = ({ singleFeature }) => {
  return (
    <div className="col-lg-4 col-sm-6">
      <div className="">
   
        <div className="support-content">
        <img
            className="animated suport"
            src={process.env.PUBLIC_URL + singleFeature.image}
            alt=""
          />
          <h5>{singleFeature.title}</h5>
          <p>{singleFeature.subtitle}</p>
        </div>
      </div>
    </div>
  );
};

FeatureIconSingle.propTypes = {
  singleFeature: PropTypes.object
};

export default FeatureIconSingle;
