import PropTypes from "prop-types";
import React from "react";
import { Link } from "react-router-dom";

const HeroSliderAdvertsSingle = ({ data, sliderClass }) => {
  return (
    <div
      className={`slider-height-9 bg-gray-2 d-flex align-items-center ${
        sliderClass ? sliderClass : ""
      }`}
    >
      <div className="container">
        <div className="row align-items-center slider-h11-mrg">
     
          <div className="col-lg-12 col-md-12 col-12 col-sm-12">
            <div className="slider-singleimg-hm11 slider-animated-1">
              <img
                className="animated adverts"
                src={process.env.PUBLIC_URL + data.image}
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

HeroSliderAdvertsSingle.propTypes = {
  data: PropTypes.object,
  sliderClass: PropTypes.string
};

export default HeroSliderAdvertsSingle;
