import PropTypes from "prop-types";
import React from "react";
import { Link } from "react-router-dom";

const HeroSliderOneSingle = ({ data, sliderClassName }) => {
  return (
    <div
      className={`single-slider slider-height-1 bg-purple ${
        sliderClassName ? sliderClassName : ""
      }`}
    >
      <div className="container">
        <div className="row">
          <div className="col-xl-4 col-lg-4 col-md-6 col-12 col-sm-4">
            <div className="slider-content slider-animated-1">
              <h1 className="animated title">{data.title}</h1>
              <div className="slider-btn btn-hover">
                <Link
                  className="animated animated-1"
                  to={process.env.PUBLIC_URL + data.url}
                >
                  PARA VOCÊ
                </Link>
              </div>
            </div>
          </div>
          <div className="col-xl-4 col-lg-4 col-md-4 col-12 col-sm-4">
            <div className="slider-single-img slider-animated-1">
              <img
                className="animated img-fluid"
                src={process.env.PUBLIC_URL + data.image}
                alt=""
              />
            </div>
          </div>
          <div className="col-xl-4 col-lg-4 col-md-4 col-12 col-sm-4">
            <div className="slider-content slider-animated-1">
              <h2 className="animated">{data.titleleft}</h2>
              <h1 className="animated subtitle">{data.subtitle}</h1>
              <h1 className="animated price-banner">{data.subtitleright}</h1>
              <div className="slider-btn btn-hover">
                <Link
                  className="animated animated-2"
                  to={process.env.PUBLIC_URL + data.url}
                >
                COMPRE AGORA
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

HeroSliderOneSingle.propTypes = {
  data: PropTypes.object,
  sliderClassName: PropTypes.string
};

export default HeroSliderOneSingle;
